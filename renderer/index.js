const riot = require('riot')

riot.mixin({
  msg: riot.observable(),
  title: 'Electron Riot App'
})

require('./tags/app.tag')
riot.mount('*')
